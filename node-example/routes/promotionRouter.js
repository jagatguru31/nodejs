const express = require('express');
const body_parser = require('body-parser');
const promotionRouter = express.Router();
promotionRouter.use(body_parser.json());

promotionRouter.route("/")
.all((req,res,next)=>{

    res.statusCode=200;
    res.setHeader('content-type','text/html');
    next();
})
.get((req,res,next)=>{
res.send("will send all the promotions to you !");
})
.post((req,res,next)=>{
    res.send("will add the promotion: "+req.body.name+" with details: "+req.body.description);
})
.put((req,res,next)=>{
    res.send("PUT operations not supported on promotions");
})
.delete((req,res,next)=>{
    res.send("deleting all the promotions!");
});

promotionRouter.route("/:PromotionId")
.all((req,res,next)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','text/plain');
    next();
})
.get((req,res,next)=>{
    res.send("will send all the promotion for Id "+req.params.PromotionId);
})
.post((req,res,next)=>{
 res.statusCode=403;
 res.send("Post not supported for promotion id "+req.params.PromotionId);
})
.put((req,res,next)=>{
    res.write("updating promotion Id "+req.params.PromotionId);
    res.end("\nwill update promotion "+req.body.name+" with details: "+req.body.description);
})
.delete((req,res,next)=>{
   res.send("deleting promotion "+req.params.PromotionId);
});

module.exports = promotionRouter;