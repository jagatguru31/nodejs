const express = require('express');
const leadersRouters = express.Router();
const bodyParser = require('body-parser');
leadersRouters.use(bodyParser.json());

leadersRouters.route("/")
.all((req,res,next)=>{
    res.statusCode=200;
    res.setHeader("Content-Type","text/plain");
    next();
})
.get((req,res,next)=>{
 res.send("Will send all the leaders!");
})
.post((req,res,next)=>{
res.write("updating leaders!\n")
res.end("for leader "+req.body.name+" added description: "+req.body.description);
})
.put((req,res,next)=>{
    res.statusCode=403;
    res.send("put is not suported for leaders");
})
.delete((req,res,next)=>{
    res.send("Deleting all the leaders! ");
});

leadersRouters.route("/:LeaderId")
.all((req,res,next)=>{
    res.statusCode=200;
    res.setHeader("COntent-Type","text/plain");
    next();
})
.get((req,res,next)=>{
    res.send("will send the details of leader "+req.params.LeaderId);
})
.post((req,res,next)=>{
    res.statusCode=403;
    res.send("post method not supported for leader "+req.params.LeaderId);
})
.put((req,res,next)=>{
    res.write("Updating leader "+req.params.LeaderId);
    res.end("\nUpdated leader "+req.body.name+" with description "+req.body.description);
})
.delete((req,res,next)=>{
    res.send("deleting the leader: "+req.params.LeaderId);
});

module.exports = leadersRouters;