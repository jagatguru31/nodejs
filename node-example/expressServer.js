const express = require('express');
const http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const dishRouter = require('./routes/dishRouter');
const promotionRouter= require('./routes/promotionRouter');
const leaderRouter = require('./routes/leaderRouter');
const hostname="localhost";
const port=3500;
const app = express();

app.use(express.static(__dirname+'/public'));
app.use(morgan('dev'));// dont need to use still it will populate the required html
app.use(bodyParser.json());


app.all("/dishes",(req,res,next)=>{
    res.statusCode=200;
    res.setHeader('Content-Type','text/plain');
    next();
});

app.use("/dishes",dishRouter);
app.use("/promotions",promotionRouter);
app.use("/leaders",leaderRouter);

app.use((req,res,next)=>{
    console.log(req.headers);
    res.statusCode=200;
    res.setHeader('Content-Type','text/html');
    res.end("<html><body><h1> this is an Express server </h1></body></html>");
});

const server = http.createServer(app);
server.listen(port,hostname,()=>{
    console.log(`server running at http://${hostname}:${port}`);
});