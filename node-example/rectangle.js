exports.perimeter= (x,y)=>{return (2*(x+y));};
exports.area = (x,y) =>{return x*y};

module.exports=(x,y,callback)=>{
    if(x<=0 || y<=0){
      setTimeout(()=>callback(new Error("rectangle dimentions should be greater than zero"),null)
      ,2000);
    }
    else{
      setTimeout(()=>
      callback(null,{
          perimeter:()=>{return 2*(x+y);},
          area:()=>{return x*y}
      })
      ,2000)
    }
}
//class type 1 in Javascript
var data = function(t,v){
    this.type=t;
    this.value=v;
    this.text=function(text){
        console.log(text);
    }
}

var d1= new data("test","test");
d1.text("hello");
console.log(d1);

// class type 2 in javascript
class type2{
    //this.hello=""; you cannot set private variables like this
    constructor(hello){// you can have only one constructor
        console.log("calling constructor");
        if(hello)
        this.hello=hello;
        else
        this.hello="hello";
    }
     
    

     sayHello() {// class in function does not need return type and function key word
        console.log(this.hello);
    }

    getHello(){
        return this.hello;
    }
}

var hello= new type2();
var hello1 = new type2("test");
hello.sayHello();
hello1.sayHello();
console.log("from get hellofunction "+hello.getHello());
console.log("from get hellofunction "+hello1.getHello());
console.log(hello);
console.log(hello1);