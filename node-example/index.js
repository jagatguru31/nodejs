var rectangle= require("./rectangle");
const http = require('http');
const fs =require('fs');
const path = require('path');
const hostname = 'localhost';
const port=3000;
var rect= {
    perimeter: (x,y)=> (2*(x+y)),
    area: (x,y)=>(x*y)
};

function solveRectange(l,b){

    console.log("Solvimg for rectangle with l = "+l+" and b= "+b);
    // if(l<=0 || b<=0){
    //     console.log("rectangle dimentions should be greater than zero");
    // }
    // else{
    //   console.log("tha area of rectangle is "+rect.area(l,b));
    //   console.log("the perimeter"+ rect.perimeter(l,b));
    //   console.log("tha area of rectangle is "+rectangle.area(l,b));
    //   console.log("the perimeter "+ rectangle.perimeter(l,b));
    // }

    rectangle(l,b,(err,res)=>{
     if(err==null){
      console.log("tha area of rectangle is "+res.area());
      console.log("the perimeter "+ res.perimeter());
     }
     else{
       console.log(err.message);
     }
    })
}

solveRectange(2,4);
solveRectange(3,5);
solveRectange(0,5);
solveRectange(-3,5);

const server=http.createServer((req,res) =>{
  console.log(req.headers);
  console.log("Request for "+req.url+" by method "+req.method);
  // res.statusCode=202;
  // res.setHeader('Content-Type','text/html');
  // res.end("<html><body><h1>Hello world</h1></body></html>");
  if(req.method=="GET"){
    var fileUrl;
    if(req.url=='/')
    fileUrl='/index.html';
    else fileUrl=req.url;

    var filePath = path.resolve("./public"+fileUrl);
    const fileExt = path.extname(filePath);
    if(fileExt=='.html')
    {
      fs.exists(filePath,(exists)=>{
       if(!exists){
         res.statusCode=404;
         res.setHeader('Content-Type','text/html');
         res.end('<html><body><h1>Error 404: '+fileUrl+' not found</h1></body></html>');
         return;
       }
       res.statusCode=200;
       res.setHeader('Content-Type','text/html');
       fs.createReadStream(filePath).pipe(res);
      })
    }
    else{
      res.statusCode=404;
      res.setHeader('Content-Type','text/html');
      res.end("<html><body><h1> Error 404: "+fileUrl+" not an HTML file</h1></body></html>");
      return;
    }
  }
  else{
    res.statusCode=404;
    res.setHeader('Content-Type','text/html');
    res.end("<html><body><h1> Error 404: "+req.method+" not a valid method</h1></body></html>");
    return;
  }
});

server.listen(port,hostname,()=>{
  console.log(`Server running at http://${hostname}:${port}`);
});